# Distro-tracker role

The role takes care of almost everything for you. The remaining steps that
you have to handle are:

* configuring the HTTPS certificate management and the corresponding
  configuration entries in the virtual host (if you use https)
* enabling the virtual host (either in nginx or in apache2)

# Role variables

## Mandatory variables

* `distro_tracker_fqdn`: the fully qualified domain name used by the
  service. This value is used as domain part of some emails but also
  as server name for the website.
* `distro_tracker_admin_email`: the email address of the admins, it
  receives automatic error reports and failed cron jobs.

## Optional variables

* `distro_tracker_srv_dir`: a directory below /srv hosting the service
  files
* `distro_tracker_contact_email`: the public contact point for the
  service, defaults to `owner@$DISTRO_TRACKER_FQDN`. It's used as
  `From` email of outgoing emails.
* `distro_tracker_mailserver`: select which mailserver to configure for,
  only postfix supported right now.
* `distro_tracker_webserver`: select which webserver to configure for,
  between "nginx" and "apache2".

# Example playbook

```
- name: Install distro-tracker
  ansible.builtin.include_role:
    name: freexian.debian_infrastructure.distro_tracker
    apply:
      tags:
        - distro-tracker
  vars:
    distro_tracker_fqdn: pkg.kali.org
    distro_tracker_admin_email: admins@kali.org
```

# License

BSD

# Author Information

Raphaël Hertzog <raphael@freexian.com>
