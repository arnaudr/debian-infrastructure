needrestart
===========

Install and configure needrestart to automatically restart services during
package upgrades.

Note that this role checks for the presence of microcode packages and disables
microcode checks when such packages are missing. Therefore, you should install
microcode updates before applying this role.

License
-------

MIT

Author Information
------------------

Helmut Grohne <helmut@freexian.com>
