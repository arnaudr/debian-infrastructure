---
# vars file for freexian.distro_info
distro_info:
  debian:
    keyring_package: debian-archive-keyring
    repositories:
      default:
        mirror: "{{ distro_info_debian_default_mirror }}"
        keyring_file: /usr/share/keyrings/debian-archive-keyring.gpg
      security:
        mirror: "{{ distro_info_debian_security_mirror }}"
        keyring_file: /usr/share/keyrings/debian-archive-keyring.gpg
      archive:
        mirror: "{{ distro_info_debian_archive_mirror }}"
        debootstrap_options: '--no-check-gpg'
      archive-security:
        mirror: "{{ distro_info_debian_archive_security_mirror }}"
        debootstrap_options: '--no-check-gpg'
    components:
      - main
      - contrib
      - non-free
    dists:
      experimental:
        parent: sid
      sid:
        aliases:
          - unstable
      bookworm:
        aliases:
          - testing
      bullseye:
        aliases:
          - stable
      bullseye-backports:
        parent: bullseye
        aliases:
          - stable-backports
      bullseye-backports-sloppy:
        parent: bullseye
        aliases:
          - stable-backports-sloppy
      bullseye-proposed-updates:
        parent: bullseye
      bullseye-security:
        parent: bullseye
        repository: security
        aliases:
          - stable-security
      bullseye-updates:
        parent: bullseye
        aliases:
          - stable-updates
      buster:
        aliases:
          - oldstable
      buster-backports:
        parent: buster
        aliases:
          - oldstable-backports
      buster-backports-sloppy:
        parent: buster
        aliases:
          - oldstable-backports-sloppy
      buster-proposed-updates:
        parent: buster
      buster-security:
        parent: buster
        repository: security
        aliases:
          - oldstable-security
        apt_codename: buster/updates
      buster-updates:
        parent: buster
        aliases:
          - oldstable-updates
      stretch:
        aliases:
          - oldoldstable
      stretch-backports:
        parent: stretch
      stretch-backports-sloppy:
        parent: stretch
      stretch-proposed-updates:
        parent: stretch
      stretch-security:
        parent: stretch
        repository: security
        apt_codename: stretch/updates
      stretch-updates:
        parent: stretch
      # Jessie is partly archived only
      jessie:
        repository: default
      jessie-backports:
        repository: archive
        parent: jessie
      jessie-backports-sloppy:
        repository: archive
        parent: jessie
      jessie-security:
        repository: security
        parent: jessie
        apt_codename: jessie/updates
      jessie-updates:
        repository: default
        parent: jessie
      # Remaining repositories are archived
      wheezy:
        repository: archive
      wheezy-backports:
        repository: archive
        parent: wheezy
      wheezy-backports-sloppy:
        repository: archive
        parent: wheezy
      wheezy-security:
        repository: security-archive
        parent: wheezy
        apt_codename: wheezy/updates
      wheezy-updates:
        repository: archive
        parent: wheezy
  freexian:
    keyring_package: freexian-archive-keyring
    keyring_file: /etc/apt/trusted.gpg.d/freexian-archive-extended-lts.gpg
    repositories:
      default:
        mirror: "{{ distro_info_freexian_default_mirror }}"
    components:
      - main
      - contrib
      - non-free
    dists:
      stretch:
        parent: "debian:stretch"
      stretch-lts:
        parent: stretch
      jessie:
        parent: "debian:jessie"
      jessie-lts:
        parent: jessie
      wheezy:
        parent: "debian:wheezy"
      wheezy-lts:
        parent: wheezy
  kali:
    keyring_package: kali-archive-keyring
    keyring_file: /usr/share/keyrings/kali-archive-keyring.gpg
    repositories:
      default:
        mirror: "{{ distro_info_kali_default_mirror }}"
      archive:
        mirror: "{{ distro_info_kali_archive_mirror }}"
    components:
      - main
      - contrib
      - non-free
    dists:
      kali-experimental:
        parent: kali-dev
      kali-dev: {}
      kali-rolling: {}
      kali-bleeding-edge:
        parent: kali-rolling
      kali-last-snapshot: {}
      kali-dev-only:
        parent: kali-dev
  ubuntu:
    keyring_package: ubuntu-keyring
    keyring_file: /usr/share/keyrings/ubuntu-archive-keyring.gpg
    repositories:
      default:
        mirror: "{{ distro_info_ubuntu_default_mirror }}"
      archive:
        mirror: "{{ distro_info_ubuntu_archive_mirror }}"
        debootstrap_options: '--no-check-gpg'
    components:
      - main
      - restricted
      - universe
      - multiverse
    dists: &ubuntu_dists
      kinetic:
        aliases:
          - devel
      kinetic-backports:
        parent: kinetic
        aliases:
          - devel-backports
      kinetic-proposed:
        parent: kinetic
        aliases:
          - devel-proposed
      kinetic-security:
        parent: kinetic
        aliases:
          - devel-security
      kinetic-updates:
        parent: kinetic
        aliases:
          - devel-updates
      jammy: {}
      jammy-backports:
        parent: jammy
      jammy-proposed:
        parent: jammy
      jammy-security:
        parent: jammy
      jammy-updates:
        parent: jammy
      impish: {}
      impish-backports:
        parent: impish
      impish-proposed:
        parent: impish
      impish-security:
        parent: impish
      impish-updates:
        parent: impish
      hirsute: {}
      hirsute-backports:
        parent: hirsute
      hirsute-proposed:
        parent: hirsute
      hirsute-security:
        parent: hirsute
      hirsute-updates:
        parent: hirsute
      focal: {}
      focal-backports:
        parent: focal
      focal-proposed:
        parent: focal
      focal-security:
        parent: focal
      focal-updates:
        parent: focal
      bionic: {}
      bionic-backports:
        parent: bionic
      bionic-proposed:
        parent: bionic
      bionic-security:
        parent: bionic
      bionic-updates:
        parent: bionic
      xenial: {}
      xenial-backports:
        parent: xenial
      xenial-proposed:
        parent: xenial
      xenial-security:
        parent: xenial
      xenial-updates:
        parent: xenial
      trusty: {}
      trusty-backports:
        parent: trusty
      trusty-proposed:
        parent: trusty
      trusty-security:
        parent: trusty
      trusty-updates:
        parent: trusty
  ubuntu-ports:
    keyring_package: ubuntu-keyring
    keyring_file: /usr/share/keyrings/ubuntu-archive-keyring.gpg
    repositories:
      default:
        mirror: "{{ distro_info_ubuntu_ports_mirror }}"
    components:
      - main
      - restricted
      - universe
      - multiverse
    dists: *ubuntu_dists
