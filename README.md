# Ansible Collection - freexian.debian_infrastructure

This collection provides many roles to deploy Debian build infrastructure.

Among the roles there are:
- `distro_info`: basic role exporting reusable data about Debian-based
  distributions
- `sbuild`: set up build chroots for use with sbuild
- `reprepro`: set up a package repository with an incoming queue
- `dirvish`: set up some backups with the dirvish tool
- `rebuildd`: set up a package build daemon with rebuildd (to be deprecated
  because rebuildd is no longer maintained)
- `distro_tracker`: set up a distro-tracker instance

The following roles should not be considered for public consumption, they
are only used internally by other roles:
- `gpg`: create and import GPG keys (used by the reprepro role)

# Authors

This collection is maintained by the Debian developers behind Freexian:
https://www.freexian.com
